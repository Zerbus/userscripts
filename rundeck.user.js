// ==UserScript==
// @name         rundeck
// @namespace    http://tampermonkey.net/
// @version      0.7
// @description  Rundeck
// @author       Michael
// @match        http://52.50.25.121:4440/*
// @match        https://awx.entrecote.privateaser.com/*
// @match        https://awx.privateaser.xyz/*
// @match        http://rundeck.entrecote.privateaser.com/*
// @match        https://rundeck.entrecote.privateaser.com/*
// @match        http://rundeck.entrecote.privateaser.com:4440/*
// @match        https://preprod.privateaser.com/*
// @match        https://www.mojito.privateaser.com/*
// @require      https://unpkg.com/vue
// @require      https://www.gstatic.com/firebasejs/4.5.0/firebase.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js
// @grant        none
// ==/UserScript==

(function () {
    'use strict';
    var config = {
        apiKey: "AIzaSyDrYdFycK-OZqVn2_zT-MFMEhy4lFrC4iY",
        authDomain: "privateaser-data.firebaseapp.com",
        databaseURL: "https://privateaser-data.firebaseio.com",
        projectId: "privateaser-data",
        storageBucket: "privateaser-data.appspot.com",
        messagingSenderId: "489551562365"
    };

    firebase.initializeApp(config);
    var database = firebase.database();
    var refBase = 'rundeck/messages/';

    window.jQuery(document).ready(function () {
        window.jQuery('head').append(
            '<style type="text/css">' +
            '    #privateaser-connexion {' +
            '    position: fixed;' +
            '    bottom: 0;' +
            '    padding: 15px;' +
            '    background-color: #2c3e50;' +
            '    border-top-right-radius: 7px;' +
            '    box-shadow: 4px -2px 23px 0 rgba(0, 0, 0, .26);' +
            '    z-index: 10;' +
            '    color: white;' +
            '}' +
            '' +
            '.privateaser-connexion-message {' +
            '    margin: 5px;' +
            '}' +
            '' +
            '.privateaser-connexion-message-button {' +
            '    border: 1px solid #a9b8cb;' +
            '    border-radius: 30px;' +
            '    padding: 0 7px 2px;' +
            '    background-color: inherit;' +
            '    margin-right: 10px;' +
            '    color: #a9b8cb;' +
            '    width: 25px;' +
            '    height: 25px;' +
            '}' +
            '' +
            '.privateaser-connexion-message-button:hover {' +
            '    border-color: white;' +
            '    color: white;' +
            '}' +
            '' +
            '.privateaser-connexion-message-username {' +
            '    margin-right: 5px;' +
            '    color: #27d8b7;' +
            '    text-transform: capitalize;' +
            '}' +
            '' +
            '.privateaser-connexion-message-form {' +
            '    display: flex;' +
            '}' +
            '' +
            '.privateaser-connexion-message-form input {' +
            '    margin: 6px 8px;' +
            '}' +
            '' +
            '.privateaser-connexion-button {' +
            '    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .26);' +
            '    max-width: 100%;' +
            '    display: inline-block;' +
            '    position: relative;' +
            '    cursor: pointer;' +
            '    min-height: 36px;' +
            '    min-width: 88px;' +
            '    line-height: 36px;' +
            '    vertical-align: middle;' +
            '    -webkit-box-align: center;' +
            '    align-items: center;' +
            '    text-align: center;' +
            '    border-radius: 2px;' +
            '    box-sizing: border-box;' +
            '    -webkit-user-select: none;' +
            '    -moz-user-select: none;' +
            '    -ms-user-select: none;' +
            '    user-select: none;' +
            '    outline: none;' +
            '    border: 0;' +
            '    padding: 0 6px;' +
            '    margin: 6px 8px;' +
            '    color: currentColor;' +
            '    white-space: nowrap;' +
            '    text-transform: uppercase;' +
            '    font-weight: 500;' +
            '    font-size: 14px;' +
            '    font-style: inherit;' +
            '    font-variant: inherit;' +
            '    font-family: inherit;' +
            '    text-decoration: none;' +
            '    overflow: hidden;' +
            '    letter-spacing: .01em;' +
            '    -webkit-transition: box-shadow .4s cubic-bezier(.25, .8, .25, 1), background-color .4s cubic-bezier(.25, .8, .25, 1);' +
            '    transition: box-shadow .4s cubic-bezier(.25, .8, .25, 1), background-color .4s cubic-bezier(.25, .8, .25, 1);' +
            '}' +
            '' +
            '.privateaser-connexion-button-primary {' +
            '    background-color: #27d8b7;' +
            '    color: black;' +
            '}' +
            '' +
            '.privateaser-connexion-button-secondary {' +
            '    background-color: white;' +
            '    color: black;' +
            '}' +
            '' +
            '.privateaser-connexion-loading {' +
            '    text-align: center;' +
            '    font-size: 1.3em;' +
            '    margin: 15px auto 25px;' +
            '}' +
            '</style>'
        );
        window.jQuery('body').append(
            '<div id="privateaser-connexion" v-if="privateaserConnectionVisible">' +
            '  <div class="privateaser-connexion-loading" v-if="!hasLoad">Chargement des messages...</div>' +
            '  <div v-for="message in messages" class="privateaser-connexion-message" v-if="messagesVisible">' +
            '    <button @click="deleteMessage(message.username)" class="privateaser-connexion-message-button">x</button>' +
            '    <span>{{ moment(message.dateTime).fromNow() }}</span>' +
            '    <span class="privateaser-connexion-message-username">' +
            '        {{ message.username }}' +
            '    </span>' +
            '    <span>{{ message.content }}</span>' +
            '  </div>' +
            '  <div class="privateaser-connexion-message-form">' +
            '    <input type="text" placeholder="pseudo" v-model="addForm.username"/>' +
            '    <input type="text" placeholder="message" v-model="addForm.content"/>' +
            '    <button @click="add()" class="privateaser-connexion-button privateaser-connexion-button-primary">Send</button>' +
            '    <button @click="messagesVisible = !messagesVisible"' +
            '            class="privateaser-connexion-button privateaser-connexion-button-secondary">' +
            '      {{ messagesVisible ? \'Hide messages\' : \'Show messages\' }}' +
            '    </button>' +
            '    <button @click="privateaserConnectionVisible = false"' +
            '            class="privateaser-connexion-button privateaser-connexion-button-secondary">' +
            '      Close' +
            '    </button>' +
            '  </div>' +
            '</div>'
        );


        new Vue({
            el: '#privateaser-connexion',
            data: {
                messages: [],
                messagesVisible: true,
                privateaserConnectionVisible: true,
                hasLoad: false,
                addForm: {
                    username: '',
                    content: ''
                }
            },
            computed: {
                moment: function () {
                    return moment;
                }
            },
            mounted: function () {
                var messagesRef = firebase.database().ref(refBase);
                var self = this;
                messagesRef.on('value', function (snapshot) {
                    self.hasLoad = true;
                    const messages = Object.values(snapshot.val());
                    self.messages = messages.sort((a, b) => (new Date(a.dateTime) > new Date(b.dateTime)) - (new Date(a.dateTime) < new Date(b.dateTime)))
                });
            },
            methods: {
                deleteMessage: function (username) {
                    database.ref(`${refBase}${username.replace(/\s/g, '_')}`).remove();
                },
                add: function () {
                    if (!this.addForm.username || !this.addForm.content) return;
                    database.ref(`${refBase}${this.addForm.username.replace(/\s/g, '_')}`).set({
                        username: this.addForm.username.replace(/\s/g, '_'),
                        content: this.addForm.content,
                        dateTime: moment().format('YYYY-MM-DD HH:mm:ss')
                    });
                    this.addForm.content = '';
                }
            }
        });
    });


})();
