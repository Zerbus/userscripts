// ==UserScript==
// @name         Attestation
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Attestation Catherine
// @author       Michael Rouart
// @match        https://media.interieur.gouv.fr/deplacement-covid-19/
// @grant        none
// ==/UserScript==

(function() {
    function initUser() {
        document.getElementById('field-firstname').value = 'Catherine';
        document.getElementById('field-lastname').value = 'Rouart';
        document.getElementById('field-birthday').value = '15/10/1956';
        document.getElementById('field-placeofbirth').value = 'Fontainebleau';
         document.getElementById('field-address').value = '113 rue de Chatou';
         document.getElementById('field-city').value = 'Colombes';
         document.getElementById('field-zipcode').value = '92700';
     }

     initUser();
})();
