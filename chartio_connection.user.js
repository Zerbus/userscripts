// ==UserScript==
// @name         chartio_connction
// @namespace    http://tampermonkey.net/
// @version      0.6
// @description  Show who use chartio
// @author       Michael Rouart
// @require      https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.18.2/babel.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.16.0/polyfill.js
// @require      https://unpkg.com/vue
// @require      https://code.jquery.com/jquery-3.2.1.min.js
// @require      https://www.gstatic.com/firebasejs/4.5.0/firebase.js
// @require      https://www.gstatic.com/firebasejs/4.5.0/firebase-database.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js
// @match        https://chartio.com/*
// @grant none
// ==/UserScript==

/* jshint ignore:start */
var inline_src = (<><![CDATA[
    /* jshint ignore:end */
    /* jshint esnext: false */
    /* jshint esversion: 6 */

    var config = {
    apiKey: "AIzaSyDrYdFycK-OZqVn2_zT-MFMEhy4lFrC4iY",
    authDomain: "privateaser-data.firebaseapp.com",
    databaseURL: "https://privateaser-data.firebaseio.com",
    projectId: "privateaser-data",
    storageBucket: "privateaser-data.appspot.com",
    messagingSenderId: "489551562365"
    };
    firebase.initializeApp(config);
    const database = firebase.database();

    $(document).ready(function() {
        $('head').append(`
<style type="text/css">
    #privateaser-connexion {
    position: fixed;
    bottom: 0;
    background-color: white;
    border-radius: 10px;
    box-shadow: 4px -2px 23px 0 rgba(0, 0, 0, .26);
}

.privateaser-connexion-message {
    margin: 5px;
}

.privateaser-connexion-message-button {
    border: 1px solid black;
    border-radius: 30px;
    padding: 0 7px 2px;
    background-color: inherit;
    margin-right: 10px;
}

.privateaser-connexion-message-username {
    margin-right: 5px;
    color: grey;
    text-transform: capitalize;
}

.privateaser-connexion-message-content-write {
    color: red;
}

.privateaser-connexion-message-content-read {
    color: grey;
}

.privateaser-connexion-message-form {

}

.privateaser-connexion-button {
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .26);
    margin-top: 16px;
    margin-bottom: 16px;
    max-width: 100%;
    display: inline-block;
    position: relative;
    cursor: pointer;
    min-height: 36px;
    min-width: 88px;
    line-height: 36px;
    vertical-align: middle;
    -webkit-box-align: center;
    align-items: center;
    text-align: center;
    border-radius: 2px;
    box-sizing: border-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    outline: none;
    border: 0;
    padding: 0 6px;
    margin: 6px 8px;
    color: currentColor;
    white-space: nowrap;
    text-transform: uppercase;
    font-weight: 500;
    font-size: 14px;
    font-style: inherit;
    font-variant: inherit;
    font-family: inherit;
    text-decoration: none;
    overflow: hidden;
    letter-spacing: .01em;
    -webkit-transition: box-shadow .4s cubic-bezier(.25, .8, .25, 1), background-color .4s cubic-bezier(.25, .8, .25, 1);
    transition: box-shadow .4s cubic-bezier(.25, .8, .25, 1), background-color .4s cubic-bezier(.25, .8, .25, 1);
}

.privateaser-connexion-button-primary {
    background-color: #27d8b7;
    color: white;
}

.privateaser-connexion-button-secondary {
    background-color: white;
    color: black;
}
</style>
`);
        $('body').append(`
<div id="privateaser-connexion" v-if="privateaserConnectionVisible">
  <div v-if="!hasLoad">Chargement des messages...</div>
  <div v-for="message in messages" class="privateaser-connexion-message" v-if="messagesVisible">
    <button @click="deleteMessage(message.username)" class="privateaser-connexion-message-button">x</button>
    <span>Le {{ moment(message.dateTime).format('DD/MM à HH:mm') }}</span>
    <span class="privateaser-connexion-message-username">
        {{ message.username }}
    </span>
    <span :class="{'privateaser-connexion-message-content-read': message.mode === 'read',
                   'privateaser-connexion-message-content-write': message.mode === 'write'}">{{ message.content }}</span>
  </div>
  <div class="privateaser-connexion-message-form">
    <input type="text" placeholder="pseudo" v-model="addForm.username"/>
    <input type="text" placeholder="message" v-model="addForm.content"/>
    <select v-model="addForm.mode"><option value="read">Can be kicked</option><option value="write">Don't kick</option></select>
    <button @click="add()" class="privateaser-connexion-button privateaser-connexion-button-primary">Send</button>
    <button @click="messagesVisible = !messagesVisible"
            class="privateaser-connexion-button privateaser-connexion-button-secondary">
      {{ messagesVisible ? 'Hide messages' : 'Show messages' }}
    </button>
    <button @click="privateaserConnectionVisible = false"
            class="privateaser-connexion-button privateaser-connexion-button-secondary">
      Close
    </button>
  </div>
</div>
`);

        const app = new Vue({
            el: '#privateaser-connexion',
            data: {
                messages: [],
                messagesVisible: true,
                privateaserConnectionVisible: true,
                hasLoad: false,
                addForm: {
                       username: '',
                       content: '',
                       mode: 'read'
                }
            },
            computed: {
                moment() {
                       return moment;
                       }
            },
            mounted() {
                var messagesRef = firebase.database().ref('messages');
                messagesRef.on('value', (snapshot) => {
                       this.hasLoad = true;
                       this.messages = snapshot.val();
                });
            },
            methods: {
                deleteMessage(username) {
                    firebase.database().ref(`messages/${username.replace(/\s/g,'_')}`).remove();
                },
                add() {
                       if(!this.addForm.username || !this.addForm.content) return;
                       firebase.database().ref(`messages/${this.addForm.username.replace(/\s/g,'_')}`).set({
                       username: this.addForm.username.replace(/\s/g,'_'),
                       content : this.addForm.content,
                       mode: this.addForm.mode,
                       dateTime : moment().format('YYYY-MM-DD HH:mm:ss')
                       });
                       this.addForm.content = '';
                }
            }
        });
    });


    /* jshint ignore:start */
]]></>).toString();
                  var c = Babel.transform(inline_src, { presets: [ "es2015", "es2016" ] });
eval(c.code);
/* jshint ignore:end */
