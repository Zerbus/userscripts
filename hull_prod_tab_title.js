// ==UserScript==
// @name         Hull Prod title
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Change the tab title to avoid confusing PROD and DEV
// @author       You
// @match        https://dashboard.hullapp.io/72320a1c/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.title = '\u2605 PROD Hull'
})();
