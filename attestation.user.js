// ==UserScript==
// @name         Attestation
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Attestation
// @author       Michael Rouart
// @match        https://media.interieur.gouv.fr/deplacement-covid-19/
// @grant        none
// ==/UserScript==

(function() {
    function initUser(user) {
         if (user === 'aurelie') {
             document.getElementById('field-firstname').value = 'Aurélie';
             document.getElementById('field-lastname').value = 'Braud';
             document.getElementById('field-birthday').value = '23/07/1983';
             document.getElementById('field-placeofbirth').value = 'Vendome';
         }
         if (user === 'michael') {
             document.getElementById('field-firstname').value = 'Michaël';
             document.getElementById('field-lastname').value = 'Rouart';
             document.getElementById('field-birthday').value = '05/07/1988';
             document.getElementById('field-placeofbirth').value = 'Paris 14ème';
         }
         document.getElementById('field-address').value = '11 rue Lafayette';
         document.getElementById('field-city').value = 'Colombes';
         document.getElementById('field-zipcode').value = '92700';
     }

     if (confirm("C'est Aurelie ?")) {
         initUser('aurelie');
     } else {
         initUser('michael');
     }
})();
